import tkinter as tk
from tkinter import messagebox

def crearreserva():
    ventana2 =tk.Tk()
    ventana2.title("Transporte universitario - La burra 2.0")
    ventana2.geometry("600x200")
    ventana2.configure(background = "dark red")
    etiqueta5 = tk.Label(ventana2,text="Elige la ruta que deseas reservar",bg = "gold",fg = "black")
    etiqueta5.config(font = ("Monotype Corsiva", 13))
    etiqueta5.grid(row=0, column=0)
    def reservanorte():
        if(messagebox.askyesno("Cono norte", "Desea hacer una reserva en el bus del cono norte?")):
            messagebox.showinfo("Reserva", "Reserva registrada!")
            salirreserva()
    def reservasur():
        if(messagebox.askyesno("Cono sur", "Desea hacer una reserva en el bus del cono sur?")):
            messagebox.showinfo("Reserva", "Reserva registrada!")
            salirreserva()
    def reservaeste():
        if(messagebox.askyesno("Cono este", "Desea hacer una reserva en el bus del cono este?")):
            messagebox.showinfo("Reserva", "Reserva registrada!")
            salirreserva()
    def reservaoeste():
        if(messagebox.askyesno("Cono oeste", "Desea hacer una reserva en el bus del cono oeste?")):
            messagebox.showinfo("Reserva", "Reserva registrada!")
            salirreserva()
    def salirreserva():
        ventana2.withdraw()
        crearprincipal()
    boton7=tk.Button(ventana2, text="Cono norte", command= reservanorte)
    boton7.grid(row=1, column=0, padx = 10, pady= 10)
    boton8=tk.Button(ventana2, text="Cono sur", command= reservasur)
    boton8.grid(row=1, column=1, padx = 10, pady= 10)
    boton9=tk.Button(ventana2, text="Cono este", command= reservaeste)
    boton9.grid(row=2, column=0, padx = 10, pady= 10)
    boton10=tk.Button(ventana2, text="Cono oeste", command= reservaoeste)
    boton10.grid(row=2, column=1, padx = 10, pady= 10)
    boton11=tk.Button(ventana2, text="Atras", command= salirreserva)
    boton11.grid(row=3, column=0, padx = 10, pady= 10)
    
def crearrutas():
    ventana3 =tk.Tk()
    ventana3.title("Transporte universitario - La burra 2.0")
    ventana3.geometry("500x800")
    ventana3.configure(background = "dark red")
    etiqueta6 = tk.Label(ventana3,text="Este es el listado de todas las rutas y sus horarios",bg = "gold",fg = "black")
    etiqueta6.config(font = ("Monotype Corsiva", 13))
    etiqueta6.grid(row=0, column=0)
    etiqueta7 = tk.Label(ventana3,text="Cono norte",bg = "lightcyan",fg = "black")
    etiqueta7.grid(row=1, column=0, pady= 30)
    etiqueta8 = tk.Label(ventana3,text="Horarios:",bg = "lightcyan",fg = "black")
    etiqueta8.grid(row=2, column=0, pady= 30)
    etiqueta9 = tk.Label(ventana3,text="Cono sur",bg = "lightcyan",fg = "black")
    etiqueta9.grid(row=3, column=0, pady= 30)
    etiqueta10 = tk.Label(ventana3,text="Horarios:",bg = "lightcyan",fg = "black")
    etiqueta10.grid(row=4, column=0, pady= 30)
    etiqueta11 = tk.Label(ventana3,text="Cono este",bg = "lightcyan",fg = "black")
    etiqueta11.grid(row=5, column=0, pady= 30)
    etiqueta12 = tk.Label(ventana3,text="Horarios:",bg = "lightcyan",fg = "black")
    etiqueta12.grid(row=6, column=0, pady= 30)
    etiqueta13 = tk.Label(ventana3,text="Cono oeste",bg = "lightcyan",fg = "black")
    etiqueta13.grid(row=7, column=0, pady= 30)
    etiqueta14 = tk.Label(ventana3,text="Horarios:",bg = "lightcyan",fg = "black")
    etiqueta14.grid(row=8, column=0, pady= 30)
    
    def salirrutas():
        ventana3.withdraw()
        crearprincipal()
    boton12=tk.Button(ventana3, text="Atras", command= salirrutas)
    boton12.grid(row=9, column=0, padx = 10, pady= 10)

def crearprincipal():
    ventana1 =tk.Tk()
    ventana1.title("Transporte universitario - La burra 2.0")
    ventana1.geometry("600x200")
    ventana1.configure(background = "dark red")
    etiqueta4 = tk.Label(ventana1,text=" Bienvenido a transporte La burra 2.0!" + " " + " Elige una opcion " ,bg = "gold",fg = "black")
    etiqueta4.config(font = ("Monotype Corsiva", 13))
    etiqueta4.grid(row=0, column=0)
    
    def salirprincipal():
        ventana1.withdraw()
        crearlogin(Usuarios)
    
    def entrarreserva():
        ventana1.withdraw()
        crearreserva()

    def entrarrutas():
        ventana1.withdraw()
        crearrutas()
        
    def entraropciones():
        ventana1.withdraw()
        crearopciones()
    boton3=tk.Button(ventana1, text="Reservar", command=entrarreserva)
    boton3.grid(row=1, column=0, padx = 10, pady= 10)
    boton4=tk.Button(ventana1, text="Ver rutas y horarios", command=entrarrutas)
    boton4.grid(row=1, column=1, padx = 10, pady= 10)
    boton5=tk.Button(ventana1, text="Opciones", command=entraropciones)
    boton5.grid(row=2, column=0, padx = 10, pady= 10)
    boton6=tk.Button(ventana1, text="Cerrar sesion",command=salirprincipal)
    boton6.grid(row=2, column=1, padx = 10, pady= 10)
    
def crearayuda():
    ventana1 =tk.Tk()
    ventana1.title("Transporte universitario - La burra 2.0")
    ventana1.geometry("600x200")
    ventana1.configure(background = "dark red")
    etiqueta4 = tk.Label(ventana1,text="Si no sabes usar la aplicacion correctamente, mira estos archivos",bg = "gold",fg = "black")
    etiqueta4.config(font = ("Monotype Corsiva", 13))
    etiqueta4.grid(row=0, column=0)
    def salirayuda():
        ventana1.withdraw()
        crearopciones()
    etiqueta5 = tk.Label(ventana1, text="Deck.js", bg = "lightcyan", fg = "black")
    etiqueta5.grid(row=1, column=0)
    etiqueta6 = tk.Label(ventana1, text="Ver pdf", bg = "lightcyan", fg = "black")
    etiqueta6.grid(row=2, column=0)
    etiqueta7 = tk.Label(ventana1, text="Video en youtube", bg = "lightcyan", fg = "black")
    etiqueta7.grid(row=3, column=0)
    boton3 = tk.Button(ventana1, text="Atras", command=salirayuda)
    boton3.grid(row=4, column=0)
    
def crearopciones():
    ventana1 =tk.Tk()
    ventana1.title("Transporte universitario - La burra 2.0")
    ventana1.geometry("600x200")
    ventana1.configure(background = "dark red")
    etiqueta4 = tk.Label(ventana1,text="Tienes alguna consulta?",bg = "gold",fg = "black")
    etiqueta4.config(font = ("Monotype Corsiva", 13))
    etiqueta4.grid(row=0, column=0)
    def saliropciones():
        ventana1.withdraw()
        crearprincipal()

    def entrarayuda():
        ventana1.withdraw()
        crearayuda()

    def entrarCreditos():
        messagebox.showinfo("Creditos", "Creado por el grupo 05, para aprender mas sobre Python")

    def entrarContacto():
        messagebox.showinfo("Contacto", "Si necesita mas informacion, llame al 987654321 o escribanos al correo transporteuni@uni.pe")

    boton3=tk.Button(ventana1, text="Ayuda", command=entrarayuda)
    boton3.grid(row=1, column=0, padx = 10, pady= 10)
    boton4=tk.Button(ventana1, text="Creditos", command=entrarCreditos)
    boton4.grid(row=1, column=1, padx = 10, pady= 10)
    boton5=tk.Button(ventana1, text="Contacto",command=entrarContacto)
    boton5.grid(row=2, column=0, padx = 10, pady= 10)
    boton6=tk.Button(ventana1, text="Atras", command=saliropciones)
    boton6.grid(row=2, column=1, padx = 10, pady= 10)
    
def crearlogin(Usuarios):
    ventana = tk.Tk()
    ventana.title("Transporte universitario - La burra 2.0")
    ventana.geometry("600x200")
    ventana.configure(background = "dark red")
    def entrarprincipal():
        contador = 0
        for i in range(len(Usuarios)):
            if (cuadro1.get()==Usuarios[i][0] and cuadro2.get()==Usuarios[i][1]):
                contador = contador + 1
                ventana.withdraw()
                crearprincipal()
        if (contador == 0):
            messagebox.showwarning("Error", "Usuario o password incorrectos")
    
    etiqueta1 = tk.Label(ventana,text=" Por favor, registre su usuario y su password ",bg = "gold" ,fg = "black")
    etiqueta1.config(font = ("Monotype Corsiva", 13))
    etiqueta1.grid(row=0, column=0)
    etiqueta2 = tk.Label(ventana,text="Usuario :",bg = "lightcyan",fg = "navy")
    etiqueta2.config(font = ("Calibri", 10))
    etiqueta2.grid(row=1, column=0, padx = 10, pady= 10)
    cuadro1=tk.Entry(ventana)
    cuadro1.grid(row=1, column=1, padx = 10, pady= 10)
    etiqueta3 = tk.Label(ventana,text="Password :",bg = "lightcyan",fg = "black")
    etiqueta3.config(font = ("Calibri", 10))
    etiqueta3.grid(row=2, column=0, padx = 10, pady= 10)
    cuadro2=tk.Entry(ventana)
    cuadro2.grid(row=2, column=1, padx = 10, pady= 10)
    cuadro2.config(show = "*")
    boton1=tk.Button(ventana, text="Ingresar", command=entrarprincipal)
    boton1.grid(row=3, column=0, padx = 10, pady= 10)
    ventana.mainloop( )
Usuarios= [["Mateo", "python", 0],["Giamnarco", "python", 0],["Patricia", "python", 0],["Crystal", "python", 0]]
usuarioactual=""
crearlogin(Usuarios)
